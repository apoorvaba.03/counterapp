
import { createStore } from 'redux';    // importing the store

import { createSlice, configureStore } from '@reduxjs/toolkit';

const counterinitialstate = { counter: 0 , showToggle:true};

const counterSlice = createSlice ({
    name: 'counter',
    initialstate: counterinitialstate,
    reducers:{
        increment(state) {
            state.counter++;
        },
        decrement(state) {
            state.counter--;
        },
        increase(state,action) {
            state.counter = state.counter + action.payload;
        },
        toggle(state) {
            state.showToggle = !state.showToggle;
        },
    }

});


// const store = createStore(counterSlice.reducer);

const intialAuth = {
    isAuthenticated:false
};

const authSlice = createSlice({
    name : 'authentication',
    initialState:intialAuth,
    reducers: {
        login(state){
            state.isAuthenticated = true;
        },
        logout(state){
            state.isAuthenticated = false;
        }

    }
});


const store = configureStore({
    reducer:{
       // counter : counterSlice.reducer, 
        auth: authSlice.reducer
    },
});


export const counterActions = counterSlice.actions; 
export const authActions = authSlice.actions;





export default store;

/*
const counterReducer = (state = intialstate, action) =>{   // reducer function (state,action) 
    if(action.type === 'increment')
    {
        return { counter : state.counter + 1,
                showToggle: state.showToggle,
        };
    }
    if(action.type === 'increase')
    {
        return{
            counter : state.counter + action.amount,
            showToggle: state.showToggle,
        };
    }


    if(action.type === 'toggle')
    {
        return{
            showToggle:!state.showToggle,
            counter:state.counter,
        };
    }
    if(action.type === 'decrement')
    {
        return { counter : state.counter - 1,
        showToggle : state.showToggle,
        };
    }
    return state;

};

const store = createStore(counterReducer); // creating the store pointing to the reducerfunction

export default store;
// exporting the directory
*/