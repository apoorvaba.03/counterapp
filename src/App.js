
import './App.css';
import Auth from './components/Auth';
import Counter from './components/Counter';
//import UserProfile from './components/UserProfile'

import { useSelector} from 'react-redux';

import Header from './components/Header';
// import Profile from './components/Profile';
import UserProfile from './components/UserProfile';
import Enter from './components/Enter';


function App() {

  const isAuth = useSelector(state => state.auth.isAuthenticated)
  console.log(isAuth);
  //const isAuth = useSelector( state => state.auth.isAuthenticated);
  return (
    <div>
        <Header />
       { !isAuth && <Auth />}
   
        {isAuth && <UserProfile />}
      
     
    </div>
  );
}

export default App;
