import classes from './Counter.module.css';

import { useSelector, useDispatch, connect } from 'react-redux';

// import { Component} from 'react';

import { counterActions } from '../store/index';


const Counter = () => {

    const dispatch = useDispatch(); //to dispatch the action to store
    const counter = useSelector(state => state.counter);    // getting the data from the store

    const show = useSelector(state => state.showToggle);

    const incrementHandler = () => {
       // dispatch( { type:'increment'})      // dispatching the action after button click
       dispatch(counterActions.increment());
    };

    const decrementHandler = () => {
       // dispatch({type:'decrement'})

       dispatch(counterActions.decrement());
    };

    const increaseHandler = () => {
      //  dispatch({type:'increase', amount:5})
        dispatch(counterActions.increase(5));

    }
const toggleCounterHandler = () => {
  //  dispatch({type:'toggle'})
  dispatch(counterActions.toggle());

};

 return (
    <main className={classes.counter}>
    <h1>Redux Counter</h1>
    { show && <div className={classes.value}>{counter}</div>}
    <div>
        <button onClick={incrementHandler}>increment</button>
        <button onClick={increaseHandler}>incrementby 5</button>
        <button onClick={decrementHandler}>decrement</button>
    </div>
    <button onClick={toggleCounterHandler} > Toggle Counter </button>
    </main>
 );
}
 export default Counter;

 /*
class Counter extends Component {

    incrementHandler() {
        this.props.increment();
    };

    decrementHandler() {
        this.props.decrement();
    };

    toggleCounterHandler() {}

    render(){
        return (
            <main className={classes.counter}>
            <h1>Redux Counter</h1>
            <div className={classes.value}>{this.props.counter}</div>
            <div>
                <button onClick={this.incrementHandler.bind(this)}>increment</button>
                <button onClick={this.decrementHandler.bind(this)}>decrement</button>
            </div>
           <button onClick={this.toggleCounterHandler} > Toggle Counter </button>
            </main>
         );
    }

}

const mapStatetoprops = (state) => {
    return{
        counter:state.counter
    };
};

const mapDispatchProps = dispatch => {
    return{
        increment: () =>dispatch ({type:'increment'}),
        decrement:() => dispatch ({type:'decrement'})
    }
}

export default connect(mapStatetoprops,mapDispatchProps)(Counter);*/